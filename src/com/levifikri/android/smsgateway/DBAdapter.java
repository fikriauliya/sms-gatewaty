package com.levifikri.android.smsgateway;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
    public static final String KEY_ROWID = "_id";
    public static final String KEY_SENDER = "sender";
    public static final String KEY_CONTENT = "content";
    public static final String KEY_SMS_CENTRE = "sms_centre";
    public static final String KEY_RECEIVED_AT = "received_at";
    
    private static final String TAG = "DBAdapter";
    
    private static final String DATABASE_NAME = "MyDB";
    private static final String DATABASE_TABLE = "smses";
    private static final int DATABASE_VERSION = 3;

    private static final String DATABASE_CREATE =
        "create table smses (_id integer primary key autoincrement, "
        + "sender text not null, content text, sms_centre text, received_at integer);";
        
    private final Context context;    

    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context ctx) 
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }
        
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
        	try {
        		db.execSQL(DATABASE_CREATE);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS smses");
            onCreate(db);
        }
    }    

    //---opens the database---
    public DBAdapter open() throws SQLException 
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---    
    public void close() 
    {
        DBHelper.close();
    }
    
    //---insert a contact into the database---
    public long insertSms(String sender, String content, String smsCentre, long receivedAt) 
    {
    	Log.i(TAG, "+Insert Sms");
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_SENDER, sender);
        initialValues.put(KEY_CONTENT, content);
        initialValues.put(KEY_SMS_CENTRE, smsCentre);
        initialValues.put(KEY_RECEIVED_AT, receivedAt);
        
        long res = db.insert(DATABASE_TABLE, null, initialValues);
        Log.i(TAG, "-Insert Sms, res: " + res);
        return res;
    }

    //---deletes a particular contact---
    public boolean deleteSms(long rowId) 
    {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    //---retrieves all the contacts---
    public Cursor getAllSmes() 
    {
        return db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_SENDER, KEY_CONTENT, KEY_SMS_CENTRE, KEY_RECEIVED_AT},
        		null, null, null, null, null);
    }

    //---retrieves a particular contact---
    public Cursor getSms(long rowId) throws SQLException 
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_SENDER, KEY_CONTENT, KEY_SMS_CENTRE, KEY_RECEIVED_AT}, 
                		KEY_ROWID + "=" + rowId, null,
                		null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    //---updates a contact---
    public boolean updateSmses(long rowId, String sender, String content, String smsCentre, long receivedAt) 
    {
        ContentValues args = new ContentValues();
        args.put(KEY_SENDER, sender);
        args.put(KEY_CONTENT, content);
        args.put(KEY_SMS_CENTRE, smsCentre);
        args.put(KEY_RECEIVED_AT, receivedAt);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }
}