package com.levifikri.android.smsgateway;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

public class EfficientAdapter extends BaseAdapter implements Filterable {
	private static final String TAG = "EfficientAdapter";
	
    private LayoutInflater mInflater;
    private Bitmap mIcon1;
    private Context context;
    private List<Sms> smses;
    private Reloadable reloadable;

    public EfficientAdapter(Context context, List<Sms> smses, Reloadable reloadable) {
      // Cache the LayoutInflate to avoid asking for a new one each time.
      mInflater = LayoutInflater.from(context);
      this.context = context;
      this.smses = smses;
      this.reloadable = reloadable;
    }

    /**
     * Make a view to hold each row.
     * 
     * @see android.widget.ListAdapter#getView(int, android.view.View,
     *      android.view.ViewGroup)
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
      // A ViewHolder keeps references to children views to avoid
      // unneccessary calls
      // to findViewById() on each row.
      ViewHolder holder;

      // When convertView is not null, we can reuse it directly, there is
      // no need
      // to reinflate it. We only inflate a new View when the convertView
      // supplied
      // by ListView is null.
      if (convertView == null) {
        convertView = mInflater.inflate(R.layout.adapter_content, null);

        // Creates a ViewHolder and store references to the two children
        // views
        // we want to bind data to.
        holder = new ViewHolder();
        holder.textLine = (TextView) convertView.findViewById(R.id.textLine);
   //     holder.iconLine = (ImageView) convertView.findViewById(R.id.iconLine);
        holder.buttonLine = (Button) convertView.findViewById(R.id.buttonLine);
        holder.textLine2 =(TextView) convertView.findViewById(R.id.textLine2);
        
        convertView.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
        	Sms sms = smses.get(position);
        	
        	String newLine = System.getProperty("line.separator");
            Toast.makeText(context, sms.getSender() + newLine + sms.getContent() + newLine + sms.getReceivedAt(), Toast.LENGTH_SHORT).show();    
          }
        });
        
        holder.buttonLine.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
        	DBAdapter db = new DBAdapter(v.getContext());
			db.open();
			if (db.deleteSms(smses.get(position).getId())) {
				Log.i(TAG, Integer.toString(position));
			}
			db.close();
			
			reloadable.reloadList(context);
            //Toast.makeText(context, "Delete-" + String.valueOf(pos), Toast.LENGTH_SHORT).show();
          }
        });
        
        convertView.setTag(holder);
      } else {
        // Get the ViewHolder back to get fast access to the TextView
        // and the ImageView.
        holder = (ViewHolder) convertView.getTag();
      }

      	// Get flag name and id
  //  String filename = "flag_" + String.valueOf(position);
  //  int id = context.getResources().getIdentifier(filename, "drawable", context.getString(R.string.package_str));

      	// Icons bound to the rows.
  //  if (id != 0x0) {
  //  mIcon1 = BitmapFactory.decodeResource(context.getResources(), id);
  //  }

      // Bind the data efficiently with the holder.
  //  holder.iconLine.setImageBitmap(mIcon1);
      
      holder.textLine.setText(smses.get(position).getSender());
      holder.textLine2.setText(smses.get(position).getContent());
      
      
      return convertView;
    }

    static class ViewHolder {
      TextView textLine;
      TextView textLine2;
      Button buttonLine;
    }

    @Override
    public Filter getFilter() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public long getItemId(int position) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public int getCount() {
      // TODO Auto-generated method stub
      return smses.size();
    }

    @Override
    public Object getItem(int position) {
      // TODO Auto-generated method stub
      return smses.get(position);
    }
    
    public interface Reloadable {
    	public void reloadList(Context context);
    }
  }
