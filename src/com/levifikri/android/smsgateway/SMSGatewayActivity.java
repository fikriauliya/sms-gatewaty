package com.levifikri.android.smsgateway;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SMSGatewayActivity extends Activity implements EfficientAdapter.Reloadable {
	IntentFilter intentFilter;
	SharedPreferences preferences;
	String preferencesName = "Config";

	private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// ---intent to filter for SMS messages received---
		intentFilter = new IntentFilter();
		intentFilter.addAction("SMS_RECEIVED_ACTION");

		// ---register the receiver---
		registerReceiver(intentReceiver, intentFilter);

		final TextView txtUrl = (TextView) findViewById(R.id.txtUrl);
		final TextView txtSyncRate = (TextView) findViewById(R.id.txtSyncRate);

		Button btnSave = (Button) findViewById(R.id.btnSave);

		btnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final String url = txtUrl.getText().toString();
				final String syncRate = txtSyncRate.getText().toString();

				preferences = getSharedPreferences(preferencesName,
						MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();

				editor.putString("url", url);
				editor.putString("syncRate", syncRate);
				editor.commit();

				Toast.makeText(getBaseContext(), "Saved", Toast.LENGTH_SHORT)
						.show();
			}
		});

		Button btnReload = (Button) findViewById(R.id.btnReload);
		btnReload.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				reloadList(v.getContext());
			}
		});

		preferences = getSharedPreferences(preferencesName, MODE_PRIVATE);
		String url = preferences.getString("url", "");
		String syncRate = preferences.getString("syncRate", "");

		txtUrl.setText(url);
		txtSyncRate.setText(syncRate);
		
		startService(new Intent(getBaseContext(), SynchronizerService.class));
	}

	public void reloadList(Context context) {
		final ListView lsUnpostedMessages = (ListView) findViewById(R.id.lsUnpostedMessages);

		List<Sms> smses = new LinkedList<Sms>();

		DBAdapter db = new DBAdapter(context);
		db.open();
		Cursor c = db.getAllSmes();
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(0);
				String sender = c.getString(1);
				String content = c.getString(2);
				String smsCentre = c.getString(3);
				Long receivedAt = c.getLong(4);
				smses.add(new Sms(id, sender, content, smsCentre,
						receivedAt));
			} while (c.moveToNext());
		}
		c.close();
		db.close();

		ListAdapter adap = new EfficientAdapter(context, smses, this);
		lsUnpostedMessages.setAdapter(adap);
	}
	
	@Override
	protected void onResume() {
		// ---register the receiver---
		// registerReceiver(intentReceiver, intentFilter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// ---unregister the receiver---
		// unregisterReceiver(intentReceiver);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// ---unregister the receiver---
		unregisterReceiver(intentReceiver);
		super.onPause();
	}
}