package com.levifikri.android.smsgateway;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

public class SynchronizerService extends Service {
	private static final String TAG = "SynchronizerService";
	private SharedPreferences preferences;
	private String preferencesName = "Config";
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, "+start service");
		
		List<Sms> smses = new LinkedList<Sms>(); 
		DBAdapter db = new DBAdapter(SynchronizerService.this.getBaseContext());
		db.open();
		Cursor c = db.getAllSmes();
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(0);
				String sender = c.getString(1);
				String content = c.getString(2);
				String smsCentre = c.getString(3);
				Long receivedAt = c.getLong(4);
				
				smses.add(new Sms(id, sender, content, smsCentre, receivedAt));
			} while (c.moveToNext());
		}
		c.close();
		db.close();
		
		new PostToServerTask().execute(smses.toArray(new Sms[0]));
		
		preferences = getSharedPreferences(preferencesName, MODE_PRIVATE);
    	String syncRate = preferences.getString("syncRate", "5");
		scheduleNextUpdate(Integer.parseInt(syncRate));
		
		Log.i(TAG, "-start service");
		
		return START_STICKY;
	}
	
	private void scheduleNextUpdate(int syncRate) {
		Intent intent = new Intent(this, this.getClass());
		PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// The update frequency should often be user configurable. This is not.
		long currentTimeMillis = System.currentTimeMillis();
		
		
		long nextUpdateTimeMillis = currentTimeMillis + syncRate
				* DateUtils.MINUTE_IN_MILLIS;
		Time nextUpdateTime = new Time();
		nextUpdateTime.set(nextUpdateTimeMillis);

		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, pendingIntent);
	}
	
	
	private class PostToServerTask extends AsyncTask<Sms, Integer, Pair<Integer, Integer>> {

		@Override
		protected Pair<Integer, Integer> doInBackground(Sms... smses) {
			List<Integer> postedIds = new LinkedList<Integer>();
			int errorCount = 0;
			
			for (Sms sms:smses) {
				Log.i(TAG, "Posting SMS => from: " + sms.getSender() + " , content: " + sms.getContent() + ", smsCentre: " + sms.getSmsCentre()+ ", receivedAt: " + sms.getReceivedAt());
				if (postSms(sms.getSender(), sms.getContent(), sms.getSmsCentre(), sms.getReceivedAt())) {
					Log.i(TAG, "Posted => id: " + sms.getId());
					postedIds.add(sms.getId());
				} else {
					Log.e(TAG, "Error posting => id: " + sms.getId());
					errorCount++;
				}
			}
				
			DBAdapter db = new DBAdapter(SynchronizerService.this.getBaseContext());
			db.open();
			for (Integer postedId:postedIds) {
				db.deleteSms(postedId.intValue());
				Log.i(TAG, "Removed => id: " + postedId);
			}
			db.close();
			
			return new Pair<Integer, Integer>(postedIds.size(), errorCount);
		}
		
		protected void onProgressUpdate(Integer... progress) {
			
		}
		
		protected void onPostExecute(Pair<Integer, Integer> result) {
			Toast.makeText(getBaseContext(), "Posted: " + result.getFirst() + ", error: " + result.getSecond(), Toast.LENGTH_SHORT).show();
		}
	}
	
	private boolean postSms(String sender, String content, String smsCentre, Long receivedAt) {
		preferences = getSharedPreferences(preferencesName, MODE_PRIVATE);
    	String url = preferences.getString("url", "");
    
    	Log.i(TAG, "posting to " + url);
    	// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(url);

	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("sender", sender));
	        nameValuePairs.add(new BasicNameValuePair("content", content));
	        nameValuePairs.add(new BasicNameValuePair("smsCentre", smsCentre));
	        nameValuePairs.add(new BasicNameValuePair("receivedAt", receivedAt.toString()));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        //TODO: check response
	        return true;
	    } catch (ClientProtocolException e) {
	    	Log.e(TAG, e.getMessage(), e);
	        return false;
	    } catch (IOException e) {
	    	Log.e(TAG, e.getMessage(), e);
	        return false;
	    }		
	}
}
