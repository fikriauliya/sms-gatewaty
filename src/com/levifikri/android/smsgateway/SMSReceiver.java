package com.levifikri.android.smsgateway;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {
	private static final String TAG = "SMSReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (bundle != null) {
			Object[] pdus = (Object[])bundle.get("pdus");
			
			DBAdapter db = new DBAdapter(context);
			db.open();
			
			String sender = "";
			String content = "";
			String smsCentre = "";
			long receivedAt = 0;
			
			for (int i=0; i < pdus.length; i++) {
				SmsMessage curMessage = SmsMessage.createFromPdu((byte[])pdus[i]);	
				
				if (i == 0) {
					sender = curMessage.getOriginatingAddress();
					content = curMessage.getMessageBody().toString();
					smsCentre = curMessage.getServiceCenterAddress();
					receivedAt = curMessage.getTimestampMillis();
				}
				
				content += curMessage.getMessageBody().toString();
			}
			long id = db.insertSms(sender, content, smsCentre, receivedAt);
			
			db.close();
			
			context.startService(new Intent(context, SynchronizerService.class));
			
            //---display the new SMS message---
            //Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
     
            //---launch the MainActivity---
//            Intent mainActivityIntent = new Intent(context, SMSGatewayActivity.class);
//            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(mainActivityIntent);            
//            
//            //---send a broadcast to update the SMS received in the activity---
//            Intent broadcastIntent = new Intent();
//            broadcastIntent.setAction("SMS_RECEIVED_ACTION");
//            broadcastIntent.putExtra("sms", str);
//            context.sendBroadcast(broadcastIntent);
		}
	}
}
