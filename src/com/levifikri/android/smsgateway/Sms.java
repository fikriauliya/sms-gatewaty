package com.levifikri.android.smsgateway;

public class Sms {
	private final Integer id;
	private final String sender;
	private final String content;
	private final String smsCentre;
	private final Long receivedAt;
	
	public Sms(Integer id, String sender, String content, String smsCentre, Long receivedAt) {
		this.id = id;
		this.sender = sender;
		this.content = content;
		this.smsCentre = smsCentre;
		this.receivedAt = receivedAt;
	}
	
	public Integer getId() { return this.id; }
	public String getSender() { return this.sender; }
	public String getContent() { return this.content; }
	public String getSmsCentre() { return this.smsCentre; }
	public Long getReceivedAt() { return this.receivedAt; }
}